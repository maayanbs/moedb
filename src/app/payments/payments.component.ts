import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { AngularFireDatabase } from '@angular/fire/database';
import { PaymentsService } from '../payments.service';

@Component({
  selector: 'app-payments',
  templateUrl: './payments.component.html',
  styleUrls: ['./payments.component.css']
})
export class PaymentsComponent implements OnInit {
  adress: string;
  user =[];
  userid;
  payment;
  name='';
  amount='';
  code='';
payments= []
  constructor(public router:Router,public authService: AuthService,public db:AngularFireDatabase,public paymentService: PaymentsService) { }

  ngOnInit()  {
  this.authService.user.subscribe(user => {
    if(!user) return;
    this.db.list('/users/'+user.uid+'/details').snapshotChanges().subscribe(
      details => {
        details.forEach(
              detail => {
                let y = detail.payload.toJSON();
                this.user.push(y);            
          }
        )
      })})
}
addPayment() {
  this.paymentService.addPayment(this.name, this.amount, this.code);
  this.name = '';
  this.amount = '';
  this.code = '';

}

}

