import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  constructor(public router:Router,public authService:AuthService) { }

  ngOnInit() {
  }
  toLogin() {
    this.router.navigate(['/login'])
  }
  toPayments() {
    this.router.navigate(['/payments'])
  }
  toLogout() {
    this.authService.logout()
    .then(value => {
      this.router.navigate(['/books'])
    })
}
}
