import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { AngularFireDatabase } from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})


export class PaymentsService {
  addPayment(name:string, amount:string, code:string){
    this.authService.user.subscribe(user =>{
      this.db.list('/users/'+code+'/payments').push({'name':name,'amount':amount, 'code':code});
    })
  }
  constructor(private authService: AuthService, private db: AngularFireDatabase) { }
}
